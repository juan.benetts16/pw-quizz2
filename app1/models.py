from django.db import models

from django.conf import settings
# Create your models here.

class Departamento(models.Model):
	nombre_dep = models.CharField(max_length = 100)
	slug = models.CharField(max_length = 100)

	def __str__(self):
		return self.nombre_dep

class Software(models.Model):
	nombre_soft = models.CharField(max_length = 50)
	funcion = models.CharField(max_length = 100)
	departamento = models.ForeignKey(Departamento, on_delete = models.CASCADE)
	slug = models.CharField(max_length = 30)

	def __str__(self):
		return self.nombre_soft


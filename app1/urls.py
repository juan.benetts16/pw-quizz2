from django.urls import path, include
from app1 import views

urlpatterns = [
	#Generics
	path('app0list2/', views.List.as_view(), name = "app0list2"),
	path('app0create2/', views.Create.as_view(), name = "app0create2"),
	path('app0create2dep/', views.CreateDep.as_view(), name = "app0create2dep"),
	path('app0update2/<int:pk>/', views.Update.as_view(), name = "app0update2"),
	path('app0update2dep/<int:pk>/', views.UpdateDep.as_view(), name = "app0update2dep"),
]
from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos y formas
from .models import Departamento, Software
from .forms import DepartamentoForm, SoftwareForm

class List(generic.ListView):
	template_name = "app1/app0list2.html"
	queryset = Software.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Software.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre_soft__icontains=query))
		return qs

class Create(generic.CreateView):
	template_name = "app1/app0create2.html"
	model = Software
	fields = [
		"nombre_soft",
		"funcion",
		"departamento",
		"slug",
	]
	success_url = reverse_lazy("list2")

class CreateDep(generic.CreateView):
	template_name = "app1/app0create2dep.html"
	model = Departamento
	fields = [
		"nombre_dep",
		"slug",
	]
	success_url = reverse_lazy("app0list2")

class Update(generic.UpdateView):
	template_name = "app1/app0update2.html"
	model = Software
	fields = [
		"nombre_soft",
		"funcion",
		"departamento",
		"slug",
	]
	success_url = reverse_lazy("app0list2")

class UpdateDep(generic.UpdateView):
	template_name = "app1/app0update2dep.html"
	model = Departamento
	fields = [
		"nombre_dep",
		"slug",
	]
	success_url = reverse_lazy("app0list2")
from django.urls import path, include
from app0login import views
from django.contrib.auth.views import logout_then_login

urlpatterns = [
	path("", views.index, name = "login"),
	path('cerrar/', logout_then_login, name='logout'),
	]
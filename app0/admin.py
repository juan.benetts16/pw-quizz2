from django.contrib import admin

# Register your models here.
from .models import Investigaciones, Categorias

@admin.register(Investigaciones)
class InvestigacionesAdmin(admin.ModelAdmin):
	list_display = [
		"titulo",
		"autor",
		"num_paginas",
		"categoria",
		"fecha",
		"slug",
		]

@admin.register(Categorias)
class CategoriasAdmin(admin.ModelAdmin):
	list_display = [
		"nombre",
		"slug",
		]
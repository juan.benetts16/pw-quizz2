from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos y formas
from .models import Investigaciones, Categorias
from .forms import InvestigacionesForm, CategoriasForm

#CRUD

class List(generic.ListView):
	template_name = "app0/list2.html"
	queryset = Investigaciones.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Investigaciones.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(titulo__icontains=query) | Q(autor__username__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "app0/detail2.html"
	model = Investigaciones

class Create(generic.CreateView):
	template_name = "app0/create2.html"
	model = Investigaciones
	fields = [
		"titulo",
		"autor",
		"num_paginas",
		"categoria",
		"fecha",
		"slug",
	]
	success_url = reverse_lazy("list2")

class Update(generic.UpdateView):
	template_name = "app0/update2.html"
	model = Investigaciones
	fields = [
		"titulo",
		"autor",
		"num_paginas",
		"categoria",
		"fecha",
		"slug",
	]
	success_url = reverse_lazy("list2")
